export class Paciente {
    public data:User;
    public message:string;
    public success:boolean;
}
  

export class User {
    public email:string;
    public password:string;
    public nick:string;
    public foto:string;
    public nombre:string;
    public puesto:string;
    public telefono:number;
    public area:string;
    public token:boolean;
}