import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {


    Swal.fire({
      text: "All is cool! Now you submit this form",
      icon: "success",
      buttonsStyling: !1,
      confirmButtonText: "Ok, got it!",
      customClass: {
          confirmButton: "btn font-weight-bold btn-light-primary"
      }
      }).then((function() {
          
      }))
    
  }

}
