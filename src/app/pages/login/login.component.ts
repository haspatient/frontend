import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/app/interface/paciente';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public paciente:User;
  public status:string;
  public message:string;

  constructor(private as:AuthService, private router:Router) {

    this.paciente = new  User();

  }

  ngOnInit(): void {

  }

  auth(form){
    this.as.auth(this.paciente).subscribe(res=>{
      console.log(res);

      this.status = 'success'
      let paciente = JSON.stringify(res.data);
      localStorage.setItem('identity',paciente);
      this.getToken();
    },err=>{
      this.status = 'error';
      this.message = err.error.message;
      localStorage.clear();
    })
  }


  getToken(){
    this.as.auth(this.paciente,true).subscribe(res=>{
      localStorage.setItem('token',res.data.toString());
      console.log(res);

      // this.router.navigate(['/Administrador']);
    },err=>{

      this.status = 'error';
      this.message = err.error.message;
      localStorage.clear();

    })
  }


}
