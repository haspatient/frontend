import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Paciente,User } from '../interface/paciente';
import { GLOBAL } from "./global";

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  public url:string;
  public identity:string;

  constructor(private http:HttpClient) { 
    this.url =  GLOBAL.url;
  }
  
  ServiveHttp(query:string,type:string='get',data = null){
    if (type=='post') {
      return this.http.post<Paciente>(query,data);
    }else if(type=='get'){
      return this.http.get<Paciente>(query);
    }else if (type=='put') {
      return this.http.put<Paciente>(query,data);
    }
  }

  auth(paciente:User,token = null):Observable <Paciente> {
    let obj = {...paciente,token};
    return this.ServiveHttp(this.url + 'login/auth','post',obj);
  }

  getIdentity(){
    let identity = localStorage.getItem('identity');
    identity == undefined ? this.identity = null: this.identity = identity;
    return this.identity;
  }








}
