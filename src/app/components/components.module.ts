import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MenuSideComponent } from './menu-side/menu-side.component';
import { ContentSideComponent } from './content-side/content-side.component';



@NgModule({
  declarations: [
    MenuSideComponent,
    ContentSideComponent
  ],
  imports: [
    CommonModule 
  ],exports:[
    MenuSideComponent, 
    ContentSideComponent
  ]
})
export class ComponentsModule { }
