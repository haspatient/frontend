import { Component, DoCheck, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { User } from './interface/paciente';
import { AuthService } from './services/auth.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  public identity:string;

    constructor(private router:Router,private as:AuthService){

    }

    ngOnInit(){
      this.getIdentity();
    }

    getIdentity(){
      this.identity = this.as.getIdentity();
      console.log(this.identity);

    }
}
